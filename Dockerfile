FROM fedora:29

MAINTAINER Ben Jones <ben.dylan.jones@cern.ch>


RUN dnf install -y dnf-plugins-core 
RUN dnf -y copr enable jsbillings/openafs
RUN dnf update -y
RUN dnf install -y krb5-workstation \
		   openafs-krb5     \
		   openafs-client   \
		   dkms-openafs 

RUN dnf install -y perl-Getopt-Long \
		   perl-Sys-Syslog  \
		   iproute   
ADD sysconfig/openafs /etc/sysconfig/openafs
ADD CellServDB /usr/vice/etc/
ADD CellServDB.dist /usr/vice/etc/
ADD CellServDB.local /usr/vice/etc/
ADD CellServDB /etc/openafs/
ADD CellServDB.dist /etc/openafs/
ADD CellServDB.local /etc/openafs/
ADD krb5.conf /etc/

ADD kernel-devel-4.19.3-300.fc29.x86_64.rpm /root/
RUN /bin/chmod 0644 /usr/vice/etc/CellServDB
RUN /bin/chmod 0644 /usr/vice/etc/CellServDB.dist
RUN /bin/chmod 0644 /usr/vice/etc/CellServDB.local
RUN /bin/chmod 0644 /etc/openafs/CellServDB
RUN /bin/chmod 0644 /etc/openafs/CellServDB.dist
RUN /bin/chmod 0644 /etc/openafs/CellServDB.local
RUN /bin/mkdir -p /afs

ADD afs-setserverprefs /usr/bin/
ADD launch.sh /launch.sh

ENTRYPOINT ["/launch.sh"]
